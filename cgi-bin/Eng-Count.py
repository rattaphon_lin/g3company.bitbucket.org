#!/usr/bin/python

# this file must be executable, use command chmod +x <filename>
# this file must be in cgi-bin directory

import csv

input_file = open('cgi-bin/Rattaphon Lin.csv')
data = csv.DictReader(input_file)  # read using DictReader

count1 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Eng':
        count1 = count1 + 1

input_file = open('cgi-bin/Suppakan Sattapongsa.csv')
data = csv.DictReader(input_file)  # read using DictReader

count2 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Eng':
        count2 = count2 + 1

input_file = open('cgi-bin/Kanokwan Phoomitiyayopab.csv')
data = csv.DictReader(input_file)  # read using DictReader

count3 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Eng':
        count3 = count3 + 1

input_file = open('cgi-bin/Aiyakupt Arbharasiri.csv')
data = csv.DictReader(input_file)  # read using DictReader

count4 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Eng':
        count4 = count4 + 1


input_file = open('cgi-bin/Sitthichai Somboon.csv')
data = csv.DictReader(input_file)  # read using DictReader
count5 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Eng':
        count5 = count5 + 1




input_file.close()

# print count


# HTML output
# just print out the text/html

print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "<title>Eng Count</title>"
print "</head>"
print "<body>"
print '<a href="http://0.0.0.0:8000/cgi-bin/home.py"> << Back </a>'
print "<h1> Eng Count </h2>"
print "<h2>Rattaphon Lin            =", count1, " (Blue Graph) </h2>"  # can use comma, do not need to use +
print "<h2>Suppakan Sattapongsa     =", count2, " (Red Graph)  </h2>"
print "<h2>Kanokwan Phoomitiyayopab =", count3, " (Green Graph)</h2>"
print "<h2>Aiyakupt Arbharasiri     =", count4, " (Black Graph)</h2>"
print "<h2>Sitthichai Somboon       =", count5, " (Yellow Graph)</h2>"
# In Python, we can use " or ' for string
print '<svg width="1000" height="1000">'

print '  <text x="120" y="420"> ',count1,' </text>'
print '  <text x="220" y="420"> ',count2,' </text>'
print '  <text x="320" y="420"> ',count3,' </text>'
print '  <text x="420" y="420"> ',count4,' </text>'
print '  <text x="520" y="420"> ',count5,' </text>'
print ' <rect x="100" y=',400 - (10*count1),' width= "50" height = ',(10*count1),' style="fill: blue" />'
print ' <rect x="200" y=',400 - (10*count2),' width= "50" height = ',(10*count2),' style="fill: red" />'
print ' <rect x="300" y=',400 - (10*count3),' width= "50" height = ',(10*count3),' style="fill: green" />'
print ' <rect x="400" y=',400 - (10*count4),' width= "50" height = ',(10*count4),' style="fill: black" />'
print ' <rect x="500" y=',400 - (10*count5),' width= "50" height = ',(10*count5),' style="fill: Yellow" />'

print '</svg>'

print "</body>"
print "</html>"
